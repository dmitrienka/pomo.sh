#!/usr/bin/env bash

current_task=$1
field="pomo"
pomo_string=$'\xf0\x9f\x8d\x85'
pomodoro_minutes=25
rest_seconds=300
notify=1
bell=1


append_pomo(){
p0=$(task _get ${current_task}.$field )
p1=$p0$pomo_string
task modify ${current_task} ${field}:${p1}
}

send_bell(){
    if [ $bell ]; then
         echo -e '\a'
    fi
}

send_done(){
    if [ $notify ]; then
        notify-send "Done!"  "$pomo_string You are doing great! Its time to rest! $pomo_string"
    fi
}

send_go(){
    if [ $notify ]; then
        notify-send "Go!"  "$pomo_string It's time to start a new pomodoro! $pomo_string"
    fi
}

pomo() {
    pic
    secs=$(($pomodoro_minutes * 60))
    while [ $secs -gt 0 ]; do
        tt=$(printf '%02dm:%02ds\n' $((secs%3600/60)) $((secs%60)))
        echo -ne " The pomodoro is runing! $tt\033[0K\r"
        sleep 1
        : $((secs--))
    done
    echo -e "\033[2K"
}

rest() {
    read -t 0.01
    secs=0
    echo -ne " Press any key to start a new pomodoro $tt\033[0K\r"
    while :
    do
        tt=$(printf '%02dm:%02ds\n' $((secs%3600/60)) $((secs%60)))
        echo -ne " Press any key to start a new pomodoro $tt\033[0K\r"
        : $((secs++))
        read -s -n 1 -t 1 key
        if [ $? -eq 0 ]
        then
            break
        fi
        if (($secs % $rest_seconds == 0)); then
            send_bell
            send_go
        fi
    done
    echo -e "\033[2K"
}


pic(){
cat <<EOF
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠛⠻⣶⡆⠀⠿⠀⣶⠒⠊⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣴⠾⠛⢹⣶⡤⢶⣿⡟⠶⠦⠄⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣠⣶⣤⣤⣤⣤⣴⠂⠸⠋⢀⣄⡉⠓⠀⠲⣶⣾⣿⣷⣄⠀⠀⠀⠀
⠀⠀⠀⢀⣾⡿⠋⠁⣠⣤⣿⡟⢀⣠⣾⣿⣿⣿⣷⣶⣤⣼⣿⣿⣿⣿⣆⠀⠀⠀
⠀⠀⠀⣾⡟⠀⣰⣿⣿⣿⣿⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀
⠀⠀⢸⡿⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀
⠀⠀⢸⡇⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀
⠀⠀⢸⣿⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀
⠀⠀⠸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁⠀⠀
⠀⠀⠀⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀
⠀⠀⠀⠀⠙⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠁⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠉⠛⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠋⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠉⠉⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
EOF
}

trap ctrl_c INT

ctrl_c(){
echo "Stopping pomodoro session..."
task stop $current_task
exit 0
}

while :
do
    task start $current_task
    pomo
    task stop $current_task
    append_pomo
    send_bell
    send_done
    rest
done
